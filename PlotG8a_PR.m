function [] = PlotG8a_PR(q,d,p,r,a1,aint,aend)
A = a1:aint:aend;
iend = length(A);
for i = 1:1:iend
    a = A(i);
    W = [0,0,d,a,0;0,0,d,0,a;r,r,0,p,p;0,0,0,0,0;0,0,0,0,0];
    [x,y]=G8_count(q,W);
    PR1(i)=y(1);
    PR2(i)=y(2);
    PR3(i)=y(3);
    PR4(i)=y(4);
    PR5(i)=y(5);
end
plot(A,PR1,'r',A,PR2,'--g',A,PR3,'k',A,PR4,'-.y',A,PR5,'b');
legend('�������� PR ������� 1','�������� PR ������� 2','�������� PR ������� 3','�������� PR ������� 4','�������� PR ������� 5');
title('PR ��� ����� G8');
xlabel('�������� ��������� alfa');
ylabel('�������� ������� PR ������');