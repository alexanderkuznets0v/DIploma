function [] = PlotG8r_LRIC(q,d,p,a,r1,rint,rend)
R = r1:rint:rend;
iend = length(R);
for i = 1:1:iend
    r = R(i);
    W = [0,0,d,a,0;0,0,d,0,a;r,r,0,p,p;0,0,0,0,0;0,0,0,0,0];
    [x,y]=G8_count(q,W);
    LRIC1(i)=x(1);
    LRIC2(i)=x(2);
    LRIC3(i)=x(3);
    LRIC4(i)=x(4);
    LRIC5(i)=x(5);
end
plot(R,LRIC1,'r',R,LRIC2,'--g',R,LRIC3,'k',R,LRIC4,'-.y',R,LRIC5,'b');
legend('�������� LRIC ������� 1','�������� LRIC ������� 2','�������� LRIC ������� 3','�������� LRIC ������� 4','�������� LRIC ������� 5');
title('LRIC ��� ����� G8');
xlabel('�������� ��������� r');
ylabel('�������� ������� LRIC ������');