function [] = PlotG7a_LRIC(q,d,p,r,a1,aint,aend)
A = a1:aint:aend;
iend = length(A);
for i = 1:1:iend
    a = A(i);
    W = [0,a,d,0,0;a,0,d,0,0;r,r,0,p,p;0,0,0,0,0;0,0,0,0,0];
    [x,y]=G7_count(q,W);
    LRIC1(i)=x(1);
    LRIC2(i)=x(2);
    LRIC3(i)=x(3);
    LRIC4(i)=x(4);
    LRIC5(i)=x(5);
end
plot(A,LRIC1,'r',A,LRIC2,'--g',A,LRIC3,'k',A,LRIC4,'-.y',A,LRIC5,'b');
legend('�������� LRIC ������� 1','�������� LRIC ������� 2','�������� LRIC ������� 3','�������� LRIC ������� 4','�������� LRIC ������� 5');
title('LRIC ��� ����� G7');
xlabel('�������� ��������� a');
ylabel('�������� ������� LRIC ������');