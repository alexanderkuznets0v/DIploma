function [] = PlotG7r_PR(q,d,p,a,r1,rint,rend)
R = r1:rint:rend;
iend = length(R);
for i = 1:1:iend
    r = R(i);
    W = [0,a,d,0,0;a,0,d,0,0;r,r,0,p,p;0,0,0,0,0;0,0,0,0,0];
    [x,y]=G7_count(q,W);
    PR1(i)=y(1);
    PR2(i)=y(2);
    PR3(i)=y(3);
    PR4(i)=y(4);
    PR5(i)=y(5);
end
plot(R,PR1,'r',R,PR2,'--g',R,PR3,'k',R,PR4,'-.y',R,PR5,'b');
legend('�������� PR ������� 1','�������� PR ������� 2','�������� PR ������� 3','�������� PR ������� 4','�������� PR ������� 5');
title('PR ��� ����� G7');
xlabel('�������� ��������� r');
ylabel('�������� ������� PR ������');